class LostAndFoundController < ApplicationController
  before_action :find_group

  def index
    render json: Post.where(group_id: @group).unsolved.order(created_at: :desc),
           each_serializer: PostWithSolvedSerializer
  end

  def create
    @post = Post.new(lost_and_found_params)
    @post.group = @group
    if @post.save
      render json: { message: 'Post created' }
    else
      render json: { message: 'Error on creation' }, status: 400
    end
  end

  def solve
    Post.find(params[:id]).solve
    render json: { message: 'Object found!' }
  end

  private

  def find_group
    @group = Group.where(type_cd: :lost_and_found).first
  end

  def lost_and_found_params
    params.require(:lost_and_found).permit(:text, :user_id)
  end
end
