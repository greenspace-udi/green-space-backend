class PostsController < ApplicationController
  def index
    render json: Post.where(group_id: params[:group_id]).order(created_at: :desc)
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      render json: { message: 'Post created' }
    else
      render json: { message: 'Error on creation' }, status: 400
    end
  end

  private

  def post_params
    params.require(:post).permit(:text, :group_id, :user_id)
  end
end
