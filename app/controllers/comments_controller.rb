class CommentsController < ApplicationController
  def index
    render json: Comment.where(post_id: params[:post_id]).order(created_at: :desc)
  end

  def create
    @comment = Comment.new(comment_params)
    if @comment.save
      render json: { message: 'Comment created' }
    else
      render json: { message: 'Error on creation' }, status: 400
    end
  end

  private

  def comment_params
    params.require(:post).permit(:text, :post_id, :user_id)
  end
end
