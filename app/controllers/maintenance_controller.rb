class MaintenanceController < ApplicationController
  before_action :find_group

  def index
    render json: Post.where(group_id: @group).unsolved.order(created_at: :desc),
           each_serializer: PostWithSolvedSerializer
  end

  def create
    @post = Post.new(maintenance_params)
    @post.group = @group
    if @post.save
      render json: { message: 'Post created' }
    else
      render json: { message: 'Error on creation' }, status: 400
    end
  end

  def solve
    Post.find(params[:id]).solve
    render json: { message: 'Maintenance Request solved' }
  end

  private

  def find_group
    @group = Group.where(type_cd: :maintenance).first
  end

  def maintenance_params
    params.require(:maintenance).permit(:text, :user_id)
  end
end
