class ApartmentsController < ApplicationController
  def create
    @apartment = Apartment.new(apartment_params)
    if @app.save
      render json: { message: 'Apartment created' }
    else
      render json: { message: 'Error on creation' }, status: 400
    end
  end

  private

  def public_space_params
    params.require(:apartment).permit(:name, :landlord_id)
  end
end
