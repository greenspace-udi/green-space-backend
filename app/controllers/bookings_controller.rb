class BookingsController < ApplicationController
  def index
    render json: Booking.where(public_space_id: params[:space_id])
                        .where('date >= ?', Date.today)
  end

  def show
    render json: Booking.find(params[:id])
  end

  def create
    @space = PublicSpace.find(params[:booking][:space_id])
    @date = Date.parse(params[:booking][:date])

    if @space.reserved(@date)
      render json: { message: 'Space reserved for the given date' }, status: 400 and return
    end

    @booking = Booking.new(booking_params)
    @booking.date = @date
    @booking.public_space = @space

    if @booking.save
      render json: { message: 'Booking created' }
    else
      render json: { message: 'Error on creation' }, status: 400
    end
  end

  private

  def booking_params
    params.require(:booking).permit(:user_id)
  end
end
