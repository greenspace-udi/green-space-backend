class UsersController < ApplicationController
  def index
    render json: User.all
  end

  def show
    render json: User.find(params[:id])
  end

  def create
    @user = User.new(user_params)
    if @user.save
      render json: { message: 'User created' }
    else
      render json: { message: 'Error on creation' }, status: 400
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :role_cd)
  end
end
