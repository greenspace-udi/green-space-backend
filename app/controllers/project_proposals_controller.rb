class ProjectProposalsController < ApplicationController
  before_action :find_group

  def index
    render json: Post.where(group_id: @group).order(created_at: :desc),
           each_serializer: PostWithUpvotesSerializer
  end

  def create
    @post = Post.new(project_proposal_params)
    @post.group = @group
    if @post.save
      render json: { message: 'Post created' }
    else
      render json: { message: 'Error on creation' }, status: 400
    end
  end

  def upvote
    Post.find(params[:id]).upvote(params[:user_id])
    render json: 'Project Proposal Upvoted'
  end

  private

  def find_group
    @group = Group.where(type_cd: :project_proposal).first
  end

  def project_proposal_params
    params.require(:project_proposal).permit(:text, :user_id)
  end
end
