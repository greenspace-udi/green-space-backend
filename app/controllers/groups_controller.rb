class GroupsController < ApplicationController
  def index
    render json: Group.all, each_serializer: GroupsIndexSerializer
  end

  def create
    @group = Group.new(group_params)
    @group.type = :general
    if @group.save
      render json: { message: 'Group created' }
    else
      render json: { message: 'Error on creation' }, status: 400
    end
  end

  private

  def group_params
    params.require(:group).permit(:name, :description)
  end
end
