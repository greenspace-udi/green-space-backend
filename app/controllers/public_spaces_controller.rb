class PublicSpacesController < ApplicationController
  def index
    render json: PublicSpace.all
  end

  def show
    render json: PublicSpace.find(params[:id])
  end

  def create
    @public_space = PublicSpace.new(public_space_params)
    @public_space.apartment = Apartment.first
    if @public_space.save
      render json: { message: 'Space created' }
    else
      render json: { message: 'Error on creation' }, status: 400
    end
  end

  private

  def public_space_params
    params.require(:public_space).permit(:name)
  end
end
