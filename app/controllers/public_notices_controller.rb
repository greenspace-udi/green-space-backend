class PublicNoticesController < ApplicationController
  before_action :find_group

  def index
    render json: Post.where(group_id: @group).order(created_at: :desc)
  end

  def create
    @post = Post.new(public_notice_params)
    @post.group = @group
    if @post.save
      render json: { message: 'Post created' }
    else
      render json: { message: 'Error on creation' }, status: 400
    end
  end

  private

  def find_group
    @group = Group.where(type_cd: :public_notices).first
  end

  def public_notice_params
    params.require(:public_notices).permit(:text, :user_id)
  end
end
