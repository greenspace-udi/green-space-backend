class User < ApplicationRecord
  as_enum :role, landlord: 1, tenant: 0
  has_many :bookings
  has_many :upvotes
end
