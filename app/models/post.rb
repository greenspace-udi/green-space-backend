class Post < ApplicationRecord
  belongs_to :group
  belongs_to :user
  has_many :comments
  has_many :upvotes

  scope :unsolved, -> { where(solved: false) }

  def solve
    update_attribute(:solved, true)
  end

  def upvote(user_id)
    upvotes << Upvote.create(post_id: self, user_id: user_id)
  end
end
