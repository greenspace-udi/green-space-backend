class Group < ActiveRecord::Base
  as_enum :type, group: 0,
                 lost_and_found: 1,
                 maintenance: 2,
                 project_proposal: 3,
                 public_notices: 4,
                 general: 5
  has_many :posts
end
