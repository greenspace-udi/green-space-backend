class PublicSpace < ApplicationRecord
  belongs_to :apartment
  has_many :bookings

  def reserved?(date)
    bookings.where(date: date)
  end
end
