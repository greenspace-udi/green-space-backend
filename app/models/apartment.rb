class Apartment < ApplicationRecord
  belongs_to :landlord, class_name: User, foreign_key: :landlord_id
  has_many :public_spaces
end
