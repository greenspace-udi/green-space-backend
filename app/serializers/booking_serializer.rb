class BookingSerializer < ActiveModel::Serializer
  attributes :id, :space, :date, :user, :date

  def space
    object.public_space
  end
end
