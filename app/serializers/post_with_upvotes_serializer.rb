class PostWithUpvotesSerializer < ActiveModel::Serializer
  attributes :id, :text, :user_id, :total_comments, :total_upvotes, :group
  has_many :upvotes
  has_many :comments

  def total_comments
    object.comments.count
  end

  def total_upvotes
    object.upvotes.count
  end
end
