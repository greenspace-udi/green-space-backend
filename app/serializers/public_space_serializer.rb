class PublicSpaceSerializer < ActiveModel::Serializer
  attributes :id, :name
end
