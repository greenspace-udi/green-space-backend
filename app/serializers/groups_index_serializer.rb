class GroupsIndexSerializer < ActiveModel::Serializer
  attributes :id, :type, :name, :description

  def type
    object.type_cd
  end
end
