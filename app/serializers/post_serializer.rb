class PostSerializer < ActiveModel::Serializer
  attributes :id, :text, :user_id, :total_comments
  has_many :comments

  def total_comments
    object.comments.count
  end
end
