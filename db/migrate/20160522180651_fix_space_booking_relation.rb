class FixSpaceBookingRelation < ActiveRecord::Migration[5.0]
  def change
    rename_column :bookings, :public_spaces_id, :public_space_id
  end
end
