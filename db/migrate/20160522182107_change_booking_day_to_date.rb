class ChangeBookingDayToDate < ActiveRecord::Migration[5.0]
  def change
    rename_column :bookings, :day, :date
  end
end
