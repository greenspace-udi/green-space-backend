class SeedDatabase < ActiveRecord::Migration[5.0]
  def up
    User.create(name: 'Greenspace Admin', role_cd: 1)

    50.times { User.create(name: Faker::Name.name, role_cd: 0) }

    Group.where(type_cd: 5).each do |group|
      12.times { Post.create(text: Faker::Lorem.paragraph, group_id: group.id, user_id: User.tenants.order("RANDOM()").first.id) }
    end

    Post.all.each do |post|
      15.times { Comment.create(text: Faker::Lorem.sentence, post_id: post.id, user_id: User.tenants.order("RANDOM()").first.id) }
    end

    Group.where(type_cd: 1).each do |group|
      5.times { Post.create(text: Faker::Lorem.paragraph, group_id: group.id, user_id: User.tenants.order("RANDOM()").first.id) }
    end

    Group.where(type_cd: 2).each do |group|
      5.times { Post.create(text: Faker::Lorem.paragraph, group_id: group.id, user_id: User.tenants.order("RANDOM()").first.id) }
    end

    Group.where(type_cd: 3).each do |group|
      5.times { Post.create(text: Faker::Lorem.paragraph, group_id: group.id, user_id: User.tenants.order("RANDOM()").first.id) }
    end

    Group.where(type_cd: 4).each do |group|
      5.times { Post.create(text: Faker::Lorem.paragraph, group_id: group.id, user_id: User.tenants.order("RANDOM()").first.id) }
    end
  end

  def down
  end
end
