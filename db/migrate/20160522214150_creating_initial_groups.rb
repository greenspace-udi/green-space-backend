class CreatingInitialGroups < ActiveRecord::Migration[5.0]
  def up
    Group.create(name: "Public Notices", description: "Important notices from landlords and tenants", type: :public_notices)
    Group.create(name: "Maintenance Requests", description: "Create and check the maintenance requests", type: :maintenance)
    Group.create(name: "Lost and Founds", description: "Lookup for missing objects", type: :lost_and_found)
    Group.create(name: "Project Proposal", description: "Propose and vote changes for everyone", type: :project_proposal)
  end

  def down
    Group.delete_all
  end
end
