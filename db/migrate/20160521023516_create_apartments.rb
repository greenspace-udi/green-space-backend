class CreateApartments < ActiveRecord::Migration[5.0]
  def change
    create_table :apartments do |t|
      t.string :address
      t.float :price
      t.integer :landlord_id
      t.integer :tenant_id

      t.timestamps
    end
  end
end
