class CreatePublicSpaces < ActiveRecord::Migration[5.0]
  def change
    create_table :public_spaces do |t|
      t.string :name
      t.references :apartment, foreign_key: true

      t.timestamps
    end
  end
end
