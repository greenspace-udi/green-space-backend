class PopulateGroups < ActiveRecord::Migration[5.0]
  def up
    Group.create(name: "John Doe's BBQ", description: "Let's break the ice with my BBQ", type: :general)
    Group.create(name: "Basketball game", description: "Let's play!", type: :general)
    Group.create(name: "Community meeting", description: "Important community meeting", type: :general)
    Group.create(name: "Cooking lessons", description: "Cooking Lessons with Larry.", type: :general)
  end

  def down
    Group.where(type_cd: 5).delete_all
  end
end
