class ChangeCommentMessageToText < ActiveRecord::Migration[5.0]
  def change
    rename_column :comments, :message, :text
  end
end
