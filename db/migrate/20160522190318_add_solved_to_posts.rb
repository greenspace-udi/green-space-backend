class AddSolvedToPosts < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :solved, :boolean
  end
end
