class ChangeReservationToBooking < ActiveRecord::Migration[5.0]
  def change
    rename_table :reservations, :bookings
  end
end
