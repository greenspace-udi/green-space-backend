class RemovingFieldFromApartment < ActiveRecord::Migration[5.0]
  def change
    remove_column :apartments, :tenant_id
  end
end
