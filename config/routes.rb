Rails.application.routes.draw do
  resources :users, only: [:index, :show, :create]
  resources :groups, only: [:index, :create]

  get 'groups/:group_id/posts', to: 'posts#index'
  get 'groups/:group_id/posts/:post_id/comments', to: 'comments#index'

  resources :posts, only: [:create]
  resources :comments, only: [:create]

  get 'spaces', to: 'public_spaces#index'
  get 'spaces/:id', to: 'public_spaces#show'
  post 'spaces', to: 'public_spaces#create'

  get 'spaces/:space_id/bookings', to: 'bookings#index'
  get 'bookings/:id', to: 'bookings#show'
  post 'bookings', to: 'bookings#create'

  get 'public_notices', to: 'public_notices#index'
  post 'public_notices', to: 'public_notices#create'

  get 'maintenance', to: 'maintenance#index'
  post 'maintenance', to: 'maintenance#create'
  get 'maintenance/:id/solve', to: 'maintenance#solve'

  get 'lost_and_found', to: 'lost_and_found#index'
  post 'lost_and_found', to: 'lost_and_found#create'
  get 'lost_and_found/:id/solve', to: 'lost_and_found#solve'

  get 'project_proposals', to: 'project_proposals#index'
  post 'project_proposals', to: 'project_proposals#create'
  get 'project_proposals/:id/upvote/:user_id', to: 'project_proposals#upvote'

  resources :apartments, only: [:create]
end
